#include "StringHasher.hpp"

//For cout etc.
#include <iostream>

//For timing.
#include <chrono>

//for strings
#include <string>
using std::string;

using std::cout;
using std::endl;

int main(int argc, char* argv[])
{
    StringHasher& bob = StringHasher::getInstance();

    string a = "hello world!";

    bob.install(a);

    nlist* result;
    result = bob.lookup("hello world!");

    cout << bob.hash("hello world!") << endl;

    if(result != NULL)
      cout << result->name << endl;
    else
        cout << "Not found" << endl;

    result = bob.lookup("fred");
    if(result != NULL)
      cout << result->name << endl;
    else
      cout << "Not found" << endl;

    return 0;
}
