#include "StackAllocator.h"

//For cout etc. 
#include <iostream>

//For malloc and comparision
#include <cstdlib>

//For timing. 
#include <chrono>

#define KB 1024
#define MB 1024*1024

using std::cout;
using std::endl;

void testAllocation();
void timeAllocation();

int main(int argc, char* argv[])
{
    testAllocation();
    timeAllocation();

    return 0;
}

void testAllocation()
{
    const int ARRAY_B_MAX = 10;
    const int ARRAY_C_MAX = 100;

    int *anIntArray;
    int *anotherIntArray;

    StackAllocator *stack = new StackAllocator(MB*500);

    cout << "Print the marker, this is the top of the stack" << endl;
    cout << stack->getMarker() << endl;

    //Allocate a block
    anIntArray = (int*)stack->alloc(sizeof(int)*10);
    cout << stack->getMarker() << endl;

    //Allocate another block. 
    anotherIntArray = (int*)stack->alloc(sizeof(int)*10);
    cout << stack->getMarker() << endl;

    //Make sure block works
    for(int i=0; i<10; ++i)
        anIntArray[i] = i;

    for(int i=0; i<10; ++i)
        cout << anIntArray[i] << endl;

    //Output stuff
    cout << "Size of int:" << sizeof(int) << endl;

    cout << "anIntArray ptr value:" << anIntArray << endl;
    cout << "anotherIntArray ptr value" << anotherIntArray << endl;

    delete stack;
}

void timeAllocation()
{

    StackAllocator *stack = new StackAllocator(MB*500);

    // Record start time
    auto start = std::chrono::high_resolution_clock::now();

    int *fred = (int*)stack->alloc(sizeof(int)*1000);

    // Record end time
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Stack allocator (1000 ints) time: " << elapsed.count() << " s\n";

    delete stack;

    // Record start time
    start = std::chrono::high_resolution_clock::now();

    int* bob = (int*)malloc(sizeof(int)*1000);

    // Record end time
    finish = std::chrono::high_resolution_clock::now();
    elapsed = finish - start;
    std::cout << "Malloc (1000 ints) time: " << elapsed.count() << " s\n";


    free(bob);
}
