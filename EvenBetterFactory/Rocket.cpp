#include "Rocket.hpp"

#include <iostream>

#include "GameObjectFactory.hpp"

Rocket::Rocket()
{

}

Rocket::~Rocket()
{

}

namespace
{

    GameObject* createRocket()
    {
        return new Rocket();
    }

    /* This code 'somehow' ensures that registration will be involved before we enter the main. */
    bool s_bRegistered = GameObjectFactory::getInstance().Register("Rocket",createRocket);
}

void Rocket::draw()
{
    std::cout << "Drawing an Rocket" << std::endl;
}

void Rocket::update()
{
    std::cout << "Updating and Rocket" << std::endl;
}
